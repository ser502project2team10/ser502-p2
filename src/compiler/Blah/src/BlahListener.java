// Generated from Blah.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BlahParser}.
 */
public interface BlahListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BlahParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(BlahParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(BlahParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(BlahParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(BlahParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funDefnWithParam}
	 * labeled alternative in {@link BlahParser#functionSt}.
	 * @param ctx the parse tree
	 */
	void enterFunDefnWithParam(BlahParser.FunDefnWithParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funDefnWithParam}
	 * labeled alternative in {@link BlahParser#functionSt}.
	 * @param ctx the parse tree
	 */
	void exitFunDefnWithParam(BlahParser.FunDefnWithParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funDefnNoParam}
	 * labeled alternative in {@link BlahParser#functionSt}.
	 * @param ctx the parse tree
	 */
	void enterFunDefnNoParam(BlahParser.FunDefnNoParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funDefnNoParam}
	 * labeled alternative in {@link BlahParser#functionSt}.
	 * @param ctx the parse tree
	 */
	void exitFunDefnNoParam(BlahParser.FunDefnNoParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funCallWithParam}
	 * labeled alternative in {@link BlahParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void enterFunCallWithParam(BlahParser.FunCallWithParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funCallWithParam}
	 * labeled alternative in {@link BlahParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void exitFunCallWithParam(BlahParser.FunCallWithParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funCallNoParam}
	 * labeled alternative in {@link BlahParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void enterFunCallNoParam(BlahParser.FunCallNoParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funCallNoParam}
	 * labeled alternative in {@link BlahParser#funcCall}.
	 * @param ctx the parse tree
	 */
	void exitFunCallNoParam(BlahParser.FunCallNoParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(BlahParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(BlahParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code startBlock}
	 * labeled alternative in {@link BlahParser#block}.
	 * @param ctx the parse tree
	 */
	void enterStartBlock(BlahParser.StartBlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code startBlock}
	 * labeled alternative in {@link BlahParser#block}.
	 * @param ctx the parse tree
	 */
	void exitStartBlock(BlahParser.StartBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code closeBlock}
	 * labeled alternative in {@link BlahParser#block}.
	 * @param ctx the parse tree
	 */
	void enterCloseBlock(BlahParser.CloseBlockContext ctx);
	/**
	 * Exit a parse tree produced by the {@code closeBlock}
	 * labeled alternative in {@link BlahParser#block}.
	 * @param ctx the parse tree
	 */
	void exitCloseBlock(BlahParser.CloseBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(BlahParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(BlahParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignDigit}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 */
	void enterAssignDigit(BlahParser.AssignDigitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignDigit}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 */
	void exitAssignDigit(BlahParser.AssignDigitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignBoolean}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 */
	void enterAssignBoolean(BlahParser.AssignBooleanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignBoolean}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 */
	void exitAssignBoolean(BlahParser.AssignBooleanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignID}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 */
	void enterAssignID(BlahParser.AssignIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignID}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 */
	void exitAssignID(BlahParser.AssignIDContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link BlahParser#ifSt}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(BlahParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link BlahParser#ifSt}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(BlahParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifElseStatement}
	 * labeled alternative in {@link BlahParser#ifSt}.
	 * @param ctx the parse tree
	 */
	void enterIfElseStatement(BlahParser.IfElseStatementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifElseStatement}
	 * labeled alternative in {@link BlahParser#ifSt}.
	 * @param ctx the parse tree
	 */
	void exitIfElseStatement(BlahParser.IfElseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#ifbody}.
	 * @param ctx the parse tree
	 */
	void enterIfbody(BlahParser.IfbodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#ifbody}.
	 * @param ctx the parse tree
	 */
	void exitIfbody(BlahParser.IfbodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#elsebody}.
	 * @param ctx the parse tree
	 */
	void enterElsebody(BlahParser.ElsebodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#elsebody}.
	 * @param ctx the parse tree
	 */
	void exitElsebody(BlahParser.ElsebodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifExprWith}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 */
	void enterIfExprWith(BlahParser.IfExprWithContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifExprWith}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 */
	void exitIfExprWith(BlahParser.IfExprWithContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifExprWithBoolValue}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 */
	void enterIfExprWithBoolValue(BlahParser.IfExprWithBoolValueContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifExprWithBoolValue}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 */
	void exitIfExprWithBoolValue(BlahParser.IfExprWithBoolValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifExprWithID}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 */
	void enterIfExprWithID(BlahParser.IfExprWithIDContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifExprWithID}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 */
	void exitIfExprWithID(BlahParser.IfExprWithIDContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#whileSt}.
	 * @param ctx the parse tree
	 */
	void enterWhileSt(BlahParser.WhileStContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#whileSt}.
	 * @param ctx the parse tree
	 */
	void exitWhileSt(BlahParser.WhileStContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#printSt}.
	 * @param ctx the parse tree
	 */
	void enterPrintSt(BlahParser.PrintStContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#printSt}.
	 * @param ctx the parse tree
	 */
	void exitPrintSt(BlahParser.PrintStContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stackPush}
	 * labeled alternative in {@link BlahParser#stackSt}.
	 * @param ctx the parse tree
	 */
	void enterStackPush(BlahParser.StackPushContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stackPush}
	 * labeled alternative in {@link BlahParser#stackSt}.
	 * @param ctx the parse tree
	 */
	void exitStackPush(BlahParser.StackPushContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stackPop}
	 * labeled alternative in {@link BlahParser#stackSt}.
	 * @param ctx the parse tree
	 */
	void enterStackPop(BlahParser.StackPopContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stackPop}
	 * labeled alternative in {@link BlahParser#stackSt}.
	 * @param ctx the parse tree
	 */
	void exitStackPop(BlahParser.StackPopContext ctx);
	/**
	 * Enter a parse tree produced by the {@code integerParam}
	 * labeled alternative in {@link BlahParser#params}.
	 * @param ctx the parse tree
	 */
	void enterIntegerParam(BlahParser.IntegerParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code integerParam}
	 * labeled alternative in {@link BlahParser#params}.
	 * @param ctx the parse tree
	 */
	void exitIntegerParam(BlahParser.IntegerParamContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanParam}
	 * labeled alternative in {@link BlahParser#params}.
	 * @param ctx the parse tree
	 */
	void enterBooleanParam(BlahParser.BooleanParamContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanParam}
	 * labeled alternative in {@link BlahParser#params}.
	 * @param ctx the parse tree
	 */
	void exitBooleanParam(BlahParser.BooleanParamContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#declStmt}.
	 * @param ctx the parse tree
	 */
	void enterDeclStmt(BlahParser.DeclStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#declStmt}.
	 * @param ctx the parse tree
	 */
	void exitDeclStmt(BlahParser.DeclStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(BlahParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(BlahParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#arith_expr}.
	 * @param ctx the parse tree
	 */
	void enterArith_expr(BlahParser.Arith_exprContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#arith_expr}.
	 * @param ctx the parse tree
	 */
	void exitArith_expr(BlahParser.Arith_exprContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void enterReturn_stmt(BlahParser.Return_stmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#return_stmt}.
	 * @param ctx the parse tree
	 */
	void exitReturn_stmt(BlahParser.Return_stmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#var}.
	 * @param ctx the parse tree
	 */
	void enterVar(BlahParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#var}.
	 * @param ctx the parse tree
	 */
	void exitVar(BlahParser.VarContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#var_one}.
	 * @param ctx the parse tree
	 */
	void enterVar_one(BlahParser.Var_oneContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#var_one}.
	 * @param ctx the parse tree
	 */
	void exitVar_one(BlahParser.Var_oneContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#var_two}.
	 * @param ctx the parse tree
	 */
	void enterVar_two(BlahParser.Var_twoContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#var_two}.
	 * @param ctx the parse tree
	 */
	void exitVar_two(BlahParser.Var_twoContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#integerDecl}.
	 * @param ctx the parse tree
	 */
	void enterIntegerDecl(BlahParser.IntegerDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#integerDecl}.
	 * @param ctx the parse tree
	 */
	void exitIntegerDecl(BlahParser.IntegerDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#booleanDecl}.
	 * @param ctx the parse tree
	 */
	void enterBooleanDecl(BlahParser.BooleanDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#booleanDecl}.
	 * @param ctx the parse tree
	 */
	void exitBooleanDecl(BlahParser.BooleanDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#digit_literal}.
	 * @param ctx the parse tree
	 */
	void enterDigit_literal(BlahParser.Digit_literalContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#digit_literal}.
	 * @param ctx the parse tree
	 */
	void exitDigit_literal(BlahParser.Digit_literalContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#bool_literal}.
	 * @param ctx the parse tree
	 */
	void enterBool_literal(BlahParser.Bool_literalContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#bool_literal}.
	 * @param ctx the parse tree
	 */
	void exitBool_literal(BlahParser.Bool_literalContext ctx);
	/**
	 * Enter a parse tree produced by {@link BlahParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(BlahParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link BlahParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(BlahParser.VariableContext ctx);
}
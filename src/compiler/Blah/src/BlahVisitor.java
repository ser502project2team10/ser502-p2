// Generated from Blah.g4 by ANTLR 4.5
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link BlahParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface BlahVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link BlahParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(BlahParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(BlahParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funDefnWithParam}
	 * labeled alternative in {@link BlahParser#functionSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunDefnWithParam(BlahParser.FunDefnWithParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funDefnNoParam}
	 * labeled alternative in {@link BlahParser#functionSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunDefnNoParam(BlahParser.FunDefnNoParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funCallWithParam}
	 * labeled alternative in {@link BlahParser#funcCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunCallWithParam(BlahParser.FunCallWithParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funCallNoParam}
	 * labeled alternative in {@link BlahParser#funcCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunCallNoParam(BlahParser.FunCallNoParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(BlahParser.BodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code startBlock}
	 * labeled alternative in {@link BlahParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStartBlock(BlahParser.StartBlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code closeBlock}
	 * labeled alternative in {@link BlahParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCloseBlock(BlahParser.CloseBlockContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStmt(BlahParser.StmtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignDigit}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignDigit(BlahParser.AssignDigitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignBoolean}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignBoolean(BlahParser.AssignBooleanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assignID}
	 * labeled alternative in {@link BlahParser#assgSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignID(BlahParser.AssignIDContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifStatement}
	 * labeled alternative in {@link BlahParser#ifSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(BlahParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifElseStatement}
	 * labeled alternative in {@link BlahParser#ifSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfElseStatement(BlahParser.IfElseStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#ifbody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfbody(BlahParser.IfbodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#elsebody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElsebody(BlahParser.ElsebodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifExprWith}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfExprWith(BlahParser.IfExprWithContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifExprWithBoolValue}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfExprWithBoolValue(BlahParser.IfExprWithBoolValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifExprWithID}
	 * labeled alternative in {@link BlahParser#condExpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfExprWithID(BlahParser.IfExprWithIDContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#whileSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileSt(BlahParser.WhileStContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#printSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintSt(BlahParser.PrintStContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stackPush}
	 * labeled alternative in {@link BlahParser#stackSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStackPush(BlahParser.StackPushContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stackPop}
	 * labeled alternative in {@link BlahParser#stackSt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStackPop(BlahParser.StackPopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code integerParam}
	 * labeled alternative in {@link BlahParser#params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerParam(BlahParser.IntegerParamContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanParam}
	 * labeled alternative in {@link BlahParser#params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanParam(BlahParser.BooleanParamContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#declStmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclStmt(BlahParser.DeclStmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(BlahParser.ExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#arith_expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArith_expr(BlahParser.Arith_exprContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#return_stmt}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturn_stmt(BlahParser.Return_stmtContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#var}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar(BlahParser.VarContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#var_one}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_one(BlahParser.Var_oneContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#var_two}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVar_two(BlahParser.Var_twoContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#integerDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerDecl(BlahParser.IntegerDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#booleanDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanDecl(BlahParser.BooleanDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#digit_literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDigit_literal(BlahParser.Digit_literalContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#bool_literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBool_literal(BlahParser.Bool_literalContext ctx);
	/**
	 * Visit a parse tree produced by {@link BlahParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(BlahParser.VariableContext ctx);
}
//Reference: The Definitive ANTLR Reference Book
//Reference: http://www.antlr.org/

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.List;

public class Main {
	public static StringBuilder sb = new StringBuilder();
	
    public static void main (String []args) throws Exception {
       System.out.println("\n Starting main");

       BlahLexer lexer = new BlahLexer(new ANTLRFileStream("F:/ANTLR/SampleProg.b"));
       BlahParser parser = new BlahParser(new CommonTokenStream(lexer));

       ParseTree tree = parser.start();
       System.out.println(tree.toStringTree(parser));
//       ParseTreeWalker walker = new ParseTreeWalker(); //this creates standard walker
       NewVisitor visitor = new NewVisitor();
//       sb = new StringBuilder();
       visitor.visit(tree);
//       System.out.println(sb.toString());
       File file = new File("SampleProg.blah");
       BufferedWriter br = new BufferedWriter(new FileWriter(file));
       br.write(sb.toString());
       br.flush();
       br.close();
//       walker.walk(new BlahVisitor(), tree);
    }

}
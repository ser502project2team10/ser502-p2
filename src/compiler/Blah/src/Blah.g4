grammar Blah;

start : (program)+  ;

program : stmt
		| functionSt
		| funcCall
		;

functionSt : 'func' ID LPAREN params (COMMA params)* RPAREN body 	#funDefnWithParam
		   | 'func' ID LPAREN RPAREN body                        	#funDefnNoParam
		   ;                    

funcCall :  ID LPAREN var RPAREN						#funCallWithParam
		 | ID LPAREN RPAREN								#funCallNoParam
		 ;
LPAREN : '(' ;

RPAREN : ')' ;

body : block (stmt)+ block ;

block : OPEN_BRACKET				#startBlock
      | CLOSE_BRACKET				#closeBlock
      ;

OPEN_BRACKET : '{';

CLOSE_BRACKET : '}';

stmt : declStmt
		|assgSt
		| ifSt
		| whileSt
		| printSt
		| stackSt
		| arith_expr
		| return_stmt
      	;

assgSt : ID '=' DIGIT 				#assignDigit
		| ID '=' BOOL_VALUE			#assignBoolean
		| ID '=' var					#assignID
		;


ifSt : 'if' LPAREN expr RPAREN body						#ifStatement
		|'if' LPAREN expr RPAREN ifbody 'else' elsebody		#ifElseStatement
		;

ifbody : body ;

elsebody : body ;
		
condExpr : expr  #ifExprWith
		| BOOL_VALUE	#ifExprWithBoolValue
		| variable	#ifExprWithID
		;
whileSt : 'while' LPAREN expr RPAREN body				
		;

printSt : 'print' LPAREN var RPAREN	;

stackSt : 'stack' DOT 'push' LPAREN var RPAREN			#stackPush
        | 'stack' DOT 'pop' LPAREN RPAREN				#stackPop
   		; 
		
params : 'int' ID 										#integerParam
	   | 'boolean' ID 									#booleanParam
	   ;

declStmt : integerDecl 									
         | booleanDecl									
         ;
expr : var_one OP var_two						
     ;

arith_expr : var EQ var_one ARITH_OP var_two				
		   ;

return_stmt : 'return' var ;

var     : digit_literal	
		| variable
		| bool_literal
		;

var_one : digit_literal	
		| variable
		| bool_literal
		;
		
var_two : digit_literal
		|variable
		|bool_literal
		;
		
integerDecl : 'int' ID EQ DIGIT 					
			;

booleanDecl : 'boolean' ID EQ BOOL_VALUE 			
			;

BOOL_VALUE : 'TRUE' | 'FALSE' | 'true'| 'false';

DOT : '.';

EQ : '=';

OP : EQEQ
	| NEQ
	| LTE
	| LT
	| GTE
	| GT
	;
	
EQEQ : '==';
NEQ : '!=';
LTE :'<=';
LT : '<';
GTE : '>=';
GT : '>';
	
ARITH_OP : ADD
		 | SUB
		 | MULT
		 | DIV
		 | MOD
		 ;

ADD : '+';
SUB : '-';
MULT : '*';
DIV : '/';
MOD : '%';

COMMA : ',' ;

digit_literal: DIGIT;

bool_literal : BOOL_VALUE;

variable : ID ;

ID : [a-zA-Z]+;

DIGIT : [0-9]+;


WS : [ \t\r\n]+ ->skip;
import org.antlr.v4.runtime.misc.NotNull;

public class NewVisitor extends BlahBaseVisitor <Integer> {
	
	@Override public Integer visitBooleanDecl(@NotNull BlahParser.BooleanDeclContext ctx){
//		System.out.println("Inside Boolean declaration");
		String id = ctx.ID().getText();
		String val = ctx.BOOL_VALUE().getText();
		Main.sb.append("VARDECL:"+id+":BOOL:"+val+"\n");
		return 0;
	}
	
	@Override public Integer visitIntegerDecl(@NotNull BlahParser.IntegerDeclContext ctx){
//		System.out.println("Inside Integer Declaration");
		String id = ctx.ID().getText();
		String val = ctx.DIGIT().getText();
		Main.sb.append("VARDECL:"+id+":NUMBER:"+val+"\n");
		return 0;
	}
	
	@Override public Integer visitArith_expr(@NotNull BlahParser.Arith_exprContext ctx){
//		System.out.println("Inside Arithmetic expression");
		String var = ctx.var().getText();
		String var1 = ctx.var_one().getText();
		String var2 = ctx.var_two().getText();
		String oper = ctx.ARITH_OP().getText();
		Main.sb.append("STMT\n");
		Main.sb.append("OPER:VAR~"+var1+":NUMBER~"+var2+":"+oper+"\n");
		Main.sb.append("ASSIGN:"+var+":POP\n");
		Main.sb.append("STMTEND\n");
		return 0;
	}
	
	@Override public Integer visitAssignDigit(@NotNull BlahParser.AssignDigitContext ctx) { 

//		System.out.println("Inside Assign Digit");

		String id = ctx.ID().getText();
		String val = ctx.DIGIT().getText();
		Main.sb.append("ASSIGN:"+id+":"+val+"\n");
		return 0;
	}
	
	@Override public Integer visitAssignID(@NotNull BlahParser.AssignIDContext ctx){
//		System.out.println("Inside Assign ID");
		
		String id = ctx.ID().getText();
		String val = ctx.var().getText();
		Main.sb.append("ASSIGN:"+id+":"+val+"\n");
		return 0;
	}
	
	@Override public Integer visitAssignBoolean(@NotNull BlahParser.AssignBooleanContext ctx){
//		System.out.println("Inside Assign ID");
		
		String id = ctx.ID().getText();
		String val = ctx.BOOL_VALUE().getText();
		Main.sb.append("ASSIGN:"+id+":"+val+"\n");
		return 0;
	}
	
	@Override public Integer visitExpr(@NotNull BlahParser.ExprContext ctx){
//		System.out.println("Inside Visit Conditional Expression ");
		
		String var1 = ctx.var_one().getText();
		String var2 = ctx.var_two().getText();
		String oper = ctx.OP().getText();
		Main.sb.append("OPER:VAR~"+var1+":NUMBER~"+var2+":"+oper+"\n");
		return 0;
	}
	
	@Override public Integer visitIfStatement(BlahParser.IfStatementContext ctx) { 
		Main.sb.append("STMT\n");
		visitExpr(ctx.expr()); 
		Main.sb.append("IF:POP\n");
		Main.sb.append("STMTEND\n");
		visitBody(ctx.body());
		Main.sb.append("ENDIF\n");
		return 0;
	}
	
	@Override public Integer visitIfElseStatement(BlahParser.IfElseStatementContext ctx) {
		Main.sb.append("STMT\n");
		visitExpr(ctx.expr()); 
		Main.sb.append("IF:POP\n");
		Main.sb.append("STMTEND\n");
		visitIfbody(ctx.ifbody());
		Main.sb.append("ELSE\n");
		visitElsebody(ctx.elsebody());
		Main.sb.append("ENDIF\n");
		return 0;
	}
	
	@Override public Integer visitWhileSt(BlahParser.WhileStContext ctx) {
//		Main.sb.append("STMT\n");
		Main.sb.append("WHILE:\n");
		Main.sb.append("STMT\n");
		visitExpr(ctx.expr()); 
		Main.sb.append("IF:POP\n");
		Main.sb.append("STMTEND\n");
		visitBody(ctx.body());
		Main.sb.append("ENDWHILE\n");
		return 0;
	}
	
	@Override public Integer visitVariable(BlahParser.VariableContext ctx) { 
		Main.sb.append("VAR~"+ctx.ID());
		return visitChildren(ctx); 
	}
	@Override public Integer visitDigit_literal(BlahParser.Digit_literalContext ctx) {
		Main.sb.append("NUMBER~"+ctx.DIGIT());
		return visitChildren(ctx); }
	
	@Override public Integer visitBool_literal(BlahParser.Bool_literalContext ctx) {
		Main.sb.append("BOOL~"+ctx.BOOL_VALUE());
		return visitChildren(ctx); }	
	
	@Override public Integer visitPrintSt(BlahParser.PrintStContext ctx) { 
		Main.sb.append("PRINT:");
		visitVar(ctx.var());
		Main.sb.append("\n");
		return 0;
	}
	
	@Override public Integer visitFunCallWithParam(@NotNull BlahParser.FunCallWithParamContext ctx){
		String name = ctx.ID().getText();
		String param = ctx.var().getText();
		Main.sb.append("CALL:"+name+":");
		visitVar(ctx.var());
		Main.sb.append("\n");
		return 0;
	}
	
	@Override public Integer visitFunCallNoParam(@NotNull BlahParser.FunCallNoParamContext ctx){
		String name = ctx.ID().getText();
		Main.sb.append("CALL:"+name+"\n");
		return 0;
	}
	
	@Override public Integer visitReturn_stmt(BlahParser.Return_stmtContext ctx) { 
		Main.sb.append("RETURN:");
		visitVar(ctx.var());
		Main.sb.append("\n");
		return 0;
	}
	
	@Override public Integer visitFunDefnWithParam(@NotNull BlahParser.FunDefnWithParamContext ctx){
		int index = 0;
		String name = ctx.ID().getText();
		String param = ctx.params().get(index).getText();
		String paramName = "";
		int indexType = -1;
		indexType =param.indexOf("int"); 
		String dataType = "int";
		if (indexType != -1)
		{
			paramName = param.substring(indexType + dataType.length());
			Main.sb.append("DECL:"+name+":"+paramName+"~NUMBER\n");
		}
		else{
			indexType = param.indexOf("boolean");
			dataType = "boolean";
			if(indexType != -1){
				paramName = param.substring(indexType + dataType.length());
				Main.sb.append("DECL:"+name+":"+paramName+"~BOOL\n");
			}
		}
//		System.out.println("Index is " + indexType  +  ",paramName: "+paramName+"\n");
		return visitChildren(ctx);
	}
	
	@Override public Integer visitFunDefnNoParam(@NotNull BlahParser.FunDefnNoParamContext ctx){
		String name = ctx.ID().getText();
		Main.sb.append("DECL:"+name+"\n");
		
		return visitChildren(ctx);
	}
}
# README #

Arizona State University

Spring 2015

SER502 Project2 Team 10:

Project:  Compiler and Virtual Machine for A Programming Language.

Programming Language: Blah

Team Members:
Ashwathanarayana Akshay,
Bahl Neeraj,
Rallabhandi Pooja,
Tandan Saurabh.

Youtube Video Link: https://youtu.be/EJw5F-Wa1ek

Installation Steps:
1. Install latest version of Java.

2. Download: http://www.antlr.org/download/antlr4.5complete.jar

   Save to your directory for 3rd party Java libraries, say C:\Javalib

3. Install ANTLR using the above downloaded jar file.

4. Add antlr4.5complete.jar to CLASSPATH

5. Create short convenient commands for the ANTLR Tool, using batch files:
antlr4.bat
java org.antlr.v4.Tool %*
grun.bat
java org.antlr.v4.runtime.misc.TestRig %*

6. Download the jar file “SER502Team10Blah.jar”


7. Unzip the jar file

8. Copy the contents of the compiler src folder and the SER502LanguageRuntime.jar to the C:\Javalib


Execution Steps:

1. Go to the command prompt on Windows or terminal on Mac.

2. Change to the following directory: C:\Javalib

3. C:\Javalib>antlr4 -visitor Blah.g4

4. C:\Javalib>javac Blah*.java

5. C:\Javalib>javac Main.java

6. C:\Javalib>java Main <sampleprogram.b>

7. Use the generated intermediate code <sampleprogram.blah> to produce the final output as follows:

   java - jar SER502LanguageRuntime.jar \src\sampleCode\<sampleprogram.blah>

8. The generated output will be displayed on the command prompt